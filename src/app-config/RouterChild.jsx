import NameUser from "../components/Name_User/NameUser";
import PostUser from "../components/Post_user/PostUser";
import CommentUser from "../components/Comment_User/CommentUser";
export default [
  {
    path: "/",
    component: NameUser,
  },
  {
    path: "/PostUser",
    component: PostUser,
  },
  {
    path: "/CommentUser",
    component: CommentUser,
  },
];
