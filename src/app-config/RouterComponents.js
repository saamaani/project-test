import React from 'react'
import {BrowserRouter as Router,Route,Switch} from 'react-router-dom'
import routes from './RouterChild'
export const RouterComponents = () => {
    return (
        <Router>
        <Switch>
            {routes.map((rout,index)=>
                 <Route
                 key={rout.path}
                 path={rout.path}
                 component={rout.component}
                 exact={typeof rout.exact === "undefined" ? true : rout.exact}
               />
            )}
            </Switch>
            </Router>
    )
}

export default RouterComponents;