import React from 'react'
import './App.css';
import RouterComponents from './app-config/RouterComponents'
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/lib/integration/react";
import { persistor, store } from "./redux";
import 'bootstrap/dist/css/bootstrap.min.css';
function App() {
  return (
    <PersistGate persistor={persistor}>
    <Provider store={store}>
    <React.Fragment>
        
          <RouterComponents/>
        
     </React.Fragment>
     </Provider>
     </PersistGate>
  );
}

export default App;
