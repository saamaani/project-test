import React from "react";

import API from "../../api_helper/base_url";
import { Style } from "./style";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { postUser } from "../../redux/action/User";
const NameUser = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [nameUserFromApi, setNameUserFromApi] = React.useState([]);
  const getNameUserList = () => {
    API.get(`users`)
      .then((res) => {
        const person = res.data;
        setNameUserFromApi(person);
      })
      .catch((error) => console.log(error));
  };
  console.log(nameUserFromApi, "newnameUserFromApi");
  React.useEffect(() => {
    getNameUserList();
  }, []);
  return (

      <Style>
        <ul>
          {nameUserFromApi.map((item) => (
            <li
              onClick={() => {
                history.push("/PostUser");
                dispatch(postUser(item));
              }}
              key={item.id}
            >
              {item.name}
            </li>
          ))}
        </ul>
      </Style>

  );
};

export default NameUser;
