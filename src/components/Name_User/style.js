import styled from "styled-components";

export const Style = styled.ul`
  ul {
    list-style: none;
    width: 100%;
    display: flex;
    justify-content: space-around;
    flex-wrap: wrap;
  }
  li {
    background-color: #6ddcdc;
    margin: 30px;
    padding: 30px 15px;
    transition: 0s;
    width: 22%;
    text-align: center;
    :hover {
      background-color: #71b7d9;
      transition: all 0.5s ease;
      cursor: pointer;
    }
  }
`;
