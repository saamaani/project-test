import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import API from "../../api_helper/base_url";
import { Style } from "./style";
const CommentUser = () => {
  const selector = useSelector((state) => state.user);
  console.log(selector, "selector");
  const [postUser, setPostUser] = useState([]);
  const allPostUser = () => {
    API.get("comments")
      .then((res) => {
        console.log(res, "restest");
        const postUserApi = res.data.filter(
          (i) => i.postId === selector.userComment.id
        );
        setPostUser(postUserApi);
      })

      .catch((error) => console.log(error));
  };
  React.useEffect(() => {
    allPostUser();
  }, []);
  return (
    <Style>
      <ul>
        {postUser.map((item) => (
          <>
            <li>
              {" "}
              <h1>{item.email}</h1>
              {item.body}
            </li>
          </>
        ))}
      </ul>
    </Style>
  );
};

export default CommentUser;
