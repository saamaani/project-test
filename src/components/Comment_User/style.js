import styled from "styled-components";

export const Style = styled.ul`
  ul {
    display: flex;
    justify-content: space-between;
    float: left;
    list-style: none;
    flex-wrap: wrap;
    li {
      padding: 30px 20px;
      width: 250px;
      border: 1px solid #6ddcdc;
      border-radius: 4px;
      margin: 0 2px;
      h1 {
        font-size: 15px;
        font-weight: bold;
      }
    }
  }
`;
