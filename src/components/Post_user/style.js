import styled from "styled-components";

export const Style = styled.ul`
  width: 100%;
  display: flex;
  justify-content: center;
  flex-wrap: wrap;

  ul {
    width: 100%;
    display: flex;
    justify-content: center;
    flex-wrap: wrap;
    li {
      box-shadow: 5px 0px 5px #888888;
      padding: 30px;
      margin-top: 50px;
      position: relative;
      transition: all 0.5s ease;
      width: 53%;
      cursor: pointer;
      :hover {
        width: 58%;
      }
    }
  }
`;
