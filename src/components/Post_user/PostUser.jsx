import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import API from "../../api_helper/base_url";
import { Style } from "./style";
import { useHistory } from "react-router-dom";
import { commentUser } from "../../redux/action/User";
const PostUser = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const selector = useSelector((state) => state.user);

  const [postUser, setPostUser] = useState([]);
  const allPostUser = () => {
    API.get("posts")
      .then((res) => {
        const postUserApi = res.data.filter(
          (i) => i.userId === selector.userPost.id
        );
        setPostUser(postUserApi);
      })

      .catch((error) => console.log(error));
  };
  React.useEffect(() => {
    allPostUser();
  }, []);
  return (
    <Style>
      <ul>
        {postUser.map((item) => (
          <li
            onClick={() => {
              history.push("/CommentUser");
              dispatch(commentUser(item));
            }}
          >
            {item.title}
          </li>
        ))}
      </ul>
    </Style>
  );
};
export default PostUser;
